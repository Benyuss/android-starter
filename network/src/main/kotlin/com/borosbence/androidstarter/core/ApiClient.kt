package com.borosbence.androidstarter.core

//import com.squareup.moshi.Moshi
//import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

//import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * TODO: Add a class header comment!
 */
object ApiClient {

    fun getClient(httpClient: OkHttpClient): Retrofit {

//        val moshi = Moshi.Builder()
//                .add(KotlinJsonAdapterFactory())
//                .build()

        return Retrofit.Builder()
                .baseUrl("") //TODO
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
    }
}