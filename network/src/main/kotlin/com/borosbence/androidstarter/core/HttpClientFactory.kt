package com.borosbence.androidstarter.core

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

/**
 * TODO: Add a class header comment!
 */
object HttpClientFactory {

    fun getClient(): OkHttpClient {
        val client = OkHttpClient.Builder()

        setDebugModeInterceptor(client)

        return client.build()
    }

    private fun setDebugModeInterceptor(client: OkHttpClient.Builder) {
        val interceptor = HttpLoggingInterceptor()

        interceptor.level = HttpLoggingInterceptor.Level.BODY
        if (android.os.Debug.isDebuggerConnected()) {
            client.addInterceptor(interceptor)
        }

    }
}