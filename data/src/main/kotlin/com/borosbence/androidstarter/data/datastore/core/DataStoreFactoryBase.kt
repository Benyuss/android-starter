package com.borosbence.androidstarter.data.datastore.core

import android.content.Context
import com.borosbence.androidstarter.domain.datastore.factory.DataStoreFactory

abstract class DataStoreFactoryBase<T>(context: Context) : DataStoreFactory<T>(
        NetworkManagerImpl(context)
)