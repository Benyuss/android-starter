package com.borosbence.androidstarter.datastore.core

import android.content.Context
import com.borosbence.androidstarter.datastore.factory.DataStoreFactory

abstract class DataStoreFactoryBase<T>(context: Context) : DataStoreFactory<T>(
        NetworkManagerImpl(context)
)