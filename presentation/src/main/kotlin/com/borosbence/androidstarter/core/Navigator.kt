package com.borosbence.androidstarter.core

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

object Navigator {

    private val defaultContentHolder: Int

    init {
        defaultContentHolder = 0 //todo replace this with a resource id
    }

    fun navigateToFragment(activity: AppCompatActivity, fragment: Fragment, idTag: String) {
        val transaction = getSupportFragmentManager(activity).beginTransaction()
        setAnimationTo(transaction)
        transaction.replace(defaultContentHolder, fragment, idTag)
        finalizeTransactionAndCommit(activity, transaction, idTag, addToBackStack = true)
    }

    private fun setAnimationTo(fragmentTransaction: FragmentTransaction) {
        fragmentTransaction.setCustomAnimations(
                android.R.anim.fade_in,
                android.R.anim.fade_out)
    }

    private fun finalizeTransactionAndCommit(activity: AppCompatActivity, fragmentTransaction: FragmentTransaction, idTag: String, addToBackStack: Boolean) {
        if (addToBackStack) {
            if (!isFragmentInStack(activity, idTag)) {
                fragmentTransaction.addToBackStack(idTag)
            }
        }
        fragmentTransaction.commit()
    }

    private fun isFragmentInStack(activity: AppCompatActivity, idTagToFind: String): Boolean {
        val fragmentManager = getSupportFragmentManager(activity)
        val foundFragment = fragmentManager.findFragmentByTag(idTagToFind)
        return foundFragment != null
    }

    private fun getSupportFragmentManager(activity: AppCompatActivity): FragmentManager {
        return activity.supportFragmentManager
    }
}