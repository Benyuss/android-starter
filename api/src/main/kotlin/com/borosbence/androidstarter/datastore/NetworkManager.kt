package com.borosbence.androidstarter.datastore

interface NetworkManager {
    fun isConnectionAvailable(): Boolean
}