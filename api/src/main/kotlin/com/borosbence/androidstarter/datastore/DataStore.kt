package com.borosbence.androidstarter.datastore

import io.reactivex.Observable

interface DataStore<T> {

    fun getOne(): Observable<T>
}