package com.borosbence.androidstarter.datastore.subtypes

import com.borosbence.androidstarter.datastore.DataStore

interface CloudDataStore<T> : DataStore<T>