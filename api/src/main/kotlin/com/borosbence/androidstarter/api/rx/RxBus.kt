package com.borosbence.androidstarter.api.rx

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * TODO: Add a class header comment!
 */
object RxBus {

    private val publisher = PublishSubject.create<Any>()

    /**
     * Emit [event] of type [Any] which can be intercepted by [listen].
     */
    fun publish(event: Any) {
        publisher.onNext(event)
    }

    /**
     * Listen for any emission previously started by [publish] and act on it.
     * This callback receives the emitted object of type [eventType].
     */
    fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)
}