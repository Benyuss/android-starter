package com.borosbence.androidstarter.api.datastore

import io.reactivex.Observable

interface DataStore<T> {

    fun getOne(): Observable<T>
}