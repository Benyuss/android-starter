package com.borosbence.androidstarter.api.datastore.subtypes

import com.borosbence.androidstarter.api.datastore.DataStore

interface CloudDataStore<T> : DataStore<T>