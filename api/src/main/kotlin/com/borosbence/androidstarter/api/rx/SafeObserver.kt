package com.borosbence.androidstarter.api.rx

import com.borosbence.androidstarter.api.event.RxExceptionEvent
import io.reactivex.observers.DisposableObserver

/**
 * TODO: Add a class header comment!
 */
abstract class SafeObserver<T> : DisposableObserver<T>() {

    override fun onComplete() {
        dispose()
    }

    override fun onNext(t: T) { /* Default implementation. Can be overridden. */
    }

    override fun onError(e: Throwable) {
        RxBus.publish(RxExceptionEvent(e))
        dispose()
    }
}