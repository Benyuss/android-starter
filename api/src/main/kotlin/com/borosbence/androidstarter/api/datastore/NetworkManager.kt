package com.borosbence.androidstarter.api.datastore

interface NetworkManager {
    fun isConnectionAvailable(): Boolean
}