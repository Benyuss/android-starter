package com.borosbence.androidstarter.api.interactor

import com.borosbence.androidstarter.api.repository.Repository
import io.reactivex.Observable

/**
 * TODO: Add a class header comment!
 */
abstract class Interactor<T>(open val repository: Repository<T>) {

    open fun getOne(): Observable<T> {
        return repository.getOne()
    }
}