package com.borosbence.androidstarter.api.presenter

interface Presenter {

    fun onStart()
    fun onStop()
}