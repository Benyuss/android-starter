package com.borosbence.androidstarter.mapper

/**
 * TODO: Add a class header comment!
 */
abstract class Mapper<in E, T> {

    abstract fun mapFrom(from: E): T

    fun mapFromList(from: List<E>): List<T> {
        return from.map { response -> mapFrom(response) }
    }
}