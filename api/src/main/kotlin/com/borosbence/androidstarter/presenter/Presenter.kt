package com.borosbence.androidstarter.presenter

interface Presenter {

    fun onStart()
    fun onStop()
}