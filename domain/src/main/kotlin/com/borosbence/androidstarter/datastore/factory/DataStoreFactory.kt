package com.borosbence.androidstarter.datastore.factory

import com.borosbence.androidstarter.datastore.DataStore
import com.borosbence.androidstarter.datastore.NetworkManager
import com.borosbence.androidstarter.exception.IllegalDataStoreStateException

abstract class DataStoreFactory<T>(private var networkManager: NetworkManager) {

    fun getDataStore(): DataStore<T> {
        when {
            networkManager.isConnectionAvailable() -> return createCloudDataStore()
            else -> throw IllegalDataStoreStateException()
        }
    }

    abstract fun createCloudDataStore(): DataStore<T>
}