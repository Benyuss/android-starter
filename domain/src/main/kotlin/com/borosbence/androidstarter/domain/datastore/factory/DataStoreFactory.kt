package com.borosbence.androidstarter.domain.datastore.factory

import com.borosbence.androidstarter.api.datastore.DataStore
import com.borosbence.androidstarter.api.datastore.NetworkManager
import com.borosbence.androidstarter.domain.exception.IllegalDataStoreStateException

abstract class DataStoreFactory<T>(private var networkManager: NetworkManager) {

    fun getDataStore(): DataStore<T> {
        when {
            networkManager.isConnectionAvailable() -> return createCloudDataStore()
            else -> throw IllegalDataStoreStateException()
        }
    }

    abstract fun createCloudDataStore(): DataStore<T>
}